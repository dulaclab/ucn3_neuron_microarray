The attached R files (braim.R and braimUtils.R) fit braim to allele-specific expression data for detecting parental bias
and the effects that the factors in the experimental design have on it. Alternatively, braim can also be used for estimating differential expression across paired samples (e.g., in phospho-s-6 pulldown data where typically total RNA is also sequenced from the same sample prior to applying the phospho-s-6 pulldown and the typical analyses are differential phospho-s-6 pulldowns across conditions relative to the corresponding total RNAs). In these cases each of the parental alleles are analogous to the pair of paired samples. 

NOTE - this is a rather rudimentary code (unlike R packages available in CRAN).
See: exampleExpressionDataInput, exampleDesign, exampleParams.yml for input examples and parameters file, respectively.
The files: ENSMUST00000024599.7.posterior.samples, ENSMUST00000125366.1.posterior.samples, ENSMUST00000024599.7.pdf, ENSMUST00000125366.1.pdf, and posterior.summary
are the outputs corresponding to this example data.

What the code does:
braim.R gets as input a parameters file that specifies the locations of the input files, parameters files for fitting braim, and output options.
The parameters file includes these fields (note the format):
Under input:
1.braimUtils - full path to the braimUtils.R file which has utility functions.
2.expression.data.file - full path to the input expression data (see exampleExpressionDataInput).
	This is a tab delimited file. The first column in each line is the transcript/gene id.
	Then, for each sample, are these values (the order of these columns is not important):
	y.hat - the natural log ratio between the expression levels of the paternal and maternal alleles
	e.hat - the associated error/uncertainty of y.hat
	y.hat.p - natural log expression level of the paternal allele
	e.hat.p - the associated error/uncertainty of y.hat.p
	y.hat.m - natural log expression level of the maternal allele
	e.hat.m - the associated error/uncertainty of y.hat.m
	
	The column names of each of these fields must be preceded by the sample name and separated by an underscore character.
	These sample names are also defined in the design table file (see below). Therefore, there has to be a one-to-one correspondence between the two.
3.design.table - full path to the input design table connecting the sample names to the experimental factors (see exampleDesign).
	This is a tab delimited file where for each sample name that appears in the expression.data.file all factors are defined in the other columns.
	Currently, this cannot be a sparse table and the model is set up only works for binary factors (i.e., factors with only two levels).
	However, this can easily be expanded - contact the authors for help.

Under model:
	factors - a comma separated list of the factors which appear in the design.table.

Under sampling:
These are values of parameters for the braim sampler. Note that they may need to be tuned per experimental design.

Under output:
	output.path - all files created as output will be saved to this location.
	print.posteriors.summary.file - boolean (1/0) indicating whether to print the summary statistics of braim (one file for all transcripts/genes in expression.data.file).
	print.posterior.samples - boolean (1/0) indicating whether to print the postrior samples of braim (one big file per each transcript/gene in expression.data.file).
	print.replicate.plot - boolean (1/0) indicating whether to print a replicates plot file (one per each transcript/gene in expression.data.file).

braim.R, which uses functions in braimUtils.R, will fit the model to each transcript/gene in expression.data.file and create the corresponding output, as defined in the parameters file.


	

