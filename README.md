# ucn3_neuron_analysis

analysis code for the mouse affy exon array data and the pS6 data.

Microarray data:  GSE161507 publicly available on Jun 1 2021
pS6 data:  GSE161552 publicly available on Nov 1 2021